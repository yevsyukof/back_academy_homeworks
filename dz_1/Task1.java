package evsyukov;

import java.util.List;

public class Task1 {

    public List<Integer> square56(List<Integer> numbers) {
        return numbers.stream()
            .map(n -> n * n + 10)
            .filter(n -> n % 10 != 5 && n % 10 != 6)
            .toList();
    }
}
