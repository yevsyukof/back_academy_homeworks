package evsyukov;

public class Task3 {

    private static final int MAX_NUM_TO_PRINT = 3;

    private static volatile int curNum = 1;

    private static final Object mutex = new Object();

    public static void printNum(int num) {
        try {
            synchronized (mutex) {
                while (curNum != num) {
                    mutex.wait();
                }
                System.out.print(num);

                curNum++;
                if (curNum > MAX_NUM_TO_PRINT) {
                    curNum = 1;
                }

                mutex.notifyAll();
            }
        } catch (InterruptedException e) {
            System.err.println(e.getLocalizedMessage());
        }
    }

    public static void main(String[] args) {
        Thread thread1 = new Thread(() -> {
            for (int i = 0; i < 6; i++) {
                printNum(1);
            }
        });
        Thread thread2 = new Thread(() -> {
            for (int i = 0; i < 6; i++) {
                printNum(2);
            }
        });
        Thread thread3 = new Thread(() -> {
            for (int i = 0; i < 6; i++) {
                printNum(3);
            }
        });

        thread1.start();
        thread2.start();
        thread3.start();

        try {
            thread1.join();
            thread2.join();
            thread3.join();
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
