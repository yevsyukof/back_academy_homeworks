package evsyukov.task4;

import java.util.concurrent.LinkedBlockingQueue;
import java.util.function.Supplier;

public class Task4 {

    public static void main(String[] args) {
        Store<Object> store = new Store<>();
        Thread supplier = new Thread(new SupplierTask<>(store, Object::new));
        Thread consumer1 = new Thread(new ConsumerTask<>(store));
        Thread consumer2 = new Thread(new ConsumerTask<>(store));

        supplier.start();
        consumer1.start();
        consumer2.start();
    }
}
