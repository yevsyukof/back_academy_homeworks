package evsyukov.task4;

import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class Store<T> {

    private static final int STORE_CAPACITY = 1024;

    private final BlockingQueue<T> itemsQueue;

    public Store() {
        itemsQueue = new LinkedBlockingQueue<>(STORE_CAPACITY);
    }

    public T getItem() throws InterruptedException {
        return itemsQueue.take();
    }

    public void putItem(T item) throws InterruptedException {
        itemsQueue.put(item);
    }
}